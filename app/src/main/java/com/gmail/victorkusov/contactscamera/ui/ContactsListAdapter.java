package com.gmail.victorkusov.contactscamera.ui;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gmail.victorkusov.contactscamera.R;
import com.gmail.victorkusov.contactscamera.model.ContactUser;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ContactsListAdapter extends RecyclerView.Adapter<ContactsListAdapter.RVHolder> {

    private List<ContactUser> mContacts;
    private IOnItemClickListener mClickListener;

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mClickListener.onClick((Integer) v.getTag());
        }
    };

    public void setData(List<ContactUser> contacts, IOnItemClickListener listener) {
        mContacts = contacts;
        mClickListener = listener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RVHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_list_contacts, parent, false);
        return new RVHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RVHolder holder, int position) {
        final ContactUser user = mContacts.get(position);

        holder.mName.setText(user.getName());
        holder.mNumbers.setText(user.getNumbers());

        Uri photoThumbnail = user.getImageThumbnail();
        Picasso picasso = Picasso.get();
        if (photoThumbnail != null) {
            picasso.load(photoThumbnail).resizeDimen(R.dimen.image_width, R.dimen.image_height).into(holder.mPhoto);
        } else {
            picasso.load(R.drawable.unknown_user).resizeDimen(R.dimen.image_width, R.dimen.image_height).into(holder.mPhoto);
        }

        holder.mCardView.setTag(position);
        holder.mCardView.setOnClickListener(mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }


    class RVHolder extends RecyclerView.ViewHolder {

        private TextView mName;
        private TextView mNumbers;
        private ImageView mPhoto;
        private CardView mCardView;


        public RVHolder(View itemView) {
            super(itemView);

            mCardView = itemView.findViewById(R.id.list_contacts_card_view);
            mName = itemView.findViewById(R.id.list_contacts_name);
            mNumbers = itemView.findViewById(R.id.list_contacts_numbers);
            mPhoto = itemView.findViewById(R.id.list_contacts_photo);
        }
    }
}
