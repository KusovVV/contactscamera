package com.gmail.victorkusov.contactscamera;

import android.graphics.Bitmap;

public interface IOnCompletedCallback {
    void onComplete();
}
