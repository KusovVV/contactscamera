package com.gmail.victorkusov.contactscamera.model;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.support.annotation.NonNull;
import android.support.v4.content.AsyncTaskLoader;

import com.gmail.victorkusov.contactscamera.R;

import java.util.ArrayList;
import java.util.List;

public class ContactsLoader extends AsyncTaskLoader<List<ContactUser>> {

    private static final String SELECTION_PHONE_NUMBERS = Phone.CONTACT_ID + " = ?";
    private static final String TAG = ContactsLoader.class.getSimpleName();


    /**
     * Stores away the application context associated with context.
     * Since Loaders can be used across multiple activities it's dangerous to
     * store the context directly; always use {@link #getContext()} to retrieve
     * the Loader's Context, don't use the constructor argument directly.
     * The Context returned by {@link #getContext} is safe to use across
     * Activity instances.
     *
     * @param context used to retrieve the application context.
     */
    public ContactsLoader(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public List<ContactUser> loadInBackground() {
        ContentResolver resolver = getContext().getContentResolver();

        Cursor cursor = resolver.query(Phone.CONTENT_URI, null, null, null, null);

        List<ContactUser> contacts = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {

                String contactId = cursor.getString(cursor.getColumnIndex(Phone.CONTACT_ID));
                ContactUser contact = new ContactUser();

                long id = Long.parseLong(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ?
                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.NAME_RAW_CONTACT_ID)) :
                        contactId);


                contact.setContactId(id);
                // imageThumbnail
                String thumbnailUri = cursor.getString(cursor.getColumnIndex(Phone.PHOTO_URI));
                contact.setImageThumbnail(thumbnailUri != null ? Uri.parse(thumbnailUri) : null);

                // large photo uri
                String photoId = cursor.getString(cursor.getColumnIndex(Phone.PHOTO_FILE_ID));
                if (photoId != null) {
                    Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(contactId));
                    Uri displayPhotoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.DISPLAY_PHOTO);
                    contact.setPhotoAddress(displayPhotoUri);
                }
                // contacts name
                contact.setName(cursor.getString(cursor.getColumnIndex(Phone.DISPLAY_NAME)));

                // contact numbers
                String numbers = getPhoneNumbers(resolver, contactId);
                contact.setNumbers(String.valueOf(numbers));


                if (!contacts.contains(contact)) {
                    contacts.add(contact);
                }
            } while (cursor.moveToNext());
            cursor.close();
        }
        return contacts;
    }

    @NonNull
    private String getPhoneNumbers(ContentResolver resolver, String contactId) {
        Cursor cursorNumbers = resolver.query(Phone.CONTENT_URI, null,
                SELECTION_PHONE_NUMBERS, new String[]{contactId}, null);

        String separator = getContext().getResources().getString(R.string.separator);
        StringBuilder numbers = new StringBuilder();
        if (cursorNumbers != null && cursorNumbers.moveToFirst()) {
            do {
                numbers.append(cursorNumbers.getString(cursorNumbers.getColumnIndex(Phone.NUMBER)))
                        .append(separator);
            } while (cursorNumbers.moveToNext());
            cursorNumbers.close();
            numbers.setLength(numbers.length() - separator.length());
        }
        return String.valueOf(numbers);
    }


}
