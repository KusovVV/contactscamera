package com.gmail.victorkusov.contactscamera.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gmail.victorkusov.contactscamera.R;
import com.gmail.victorkusov.contactscamera.model.ContactUser;
import com.gmail.victorkusov.contactscamera.model.CursorContactsModel;

import java.util.List;

public class ContactsListFragment extends Fragment {

    public static final String TAG = ContactsListFragment.class.getSimpleName();

    private RecyclerView mContactsList;
    private ContactsListAdapter mAdapter;

    private CursorContactsModel mModel;
    private Observer<List<ContactUser>> mDataObserver = new Observer<List<ContactUser>>() {
        @Override
        public void onChanged(@Nullable List<ContactUser> contactUsers) {
            Log.d(TAG, "onChanged: ");
            mAdapter = (ContactsListAdapter) mContactsList.getAdapter();
            if (mAdapter == null) {
                mAdapter = new ContactsListAdapter();
            }
            mAdapter.setData(contactUsers, new IOnItemClickListener() {
                @Override
                public void onClick(int position) {

                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    CameraFragment instance = (CameraFragment) manager.findFragmentByTag(CameraFragment.TAG);
                    if (instance == null) {
                        instance = CameraFragment.newInstance(position);
                    }
                    manager.beginTransaction()
                            .replace(R.id.lay_frame, instance, CameraFragment.TAG).addToBackStack(null)
                            .commit();
                }
            });
            mContactsList.setAdapter(mAdapter);
        }
    };

    public ContactsListFragment() {
        super();
    }

    public static ContactsListFragment newInstance() {
        ContactsListFragment instance = new ContactsListFragment();

        return instance;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_list_contacts, container, false);
        mContactsList = view.findViewById(R.id.container_list_contacts);
        mContactsList.setLayoutManager(new LinearLayoutManager(inflater.getContext()));
        mModel = ViewModelProviders.of(getActivity()).get(CursorContactsModel.class);
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        mModel.getContacts().observe(this, mDataObserver);
    }

    @Override
    public void onStop() {
        if (mDataObserver != null) {
            mModel.getContacts().removeObserver(mDataObserver);
        }
        super.onStop();
    }
}
