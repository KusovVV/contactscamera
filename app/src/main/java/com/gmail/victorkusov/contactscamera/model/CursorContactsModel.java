package com.gmail.victorkusov.contactscamera.model;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

public class CursorContactsModel extends ViewModel {


    private MutableLiveData<List<ContactUser>> mContacts;

    public MutableLiveData<List<ContactUser>> getContacts() {
        if (mContacts == null) {
            mContacts = new MutableLiveData<>();
        }
        return mContacts;
    }

    public void setContact(int position, ContactUser user) {
        List<ContactUser> users = getContacts().getValue();
        users.set(position, user);
        getContacts().postValue(users);
    }
}
