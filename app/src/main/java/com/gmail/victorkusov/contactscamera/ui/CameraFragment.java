package com.gmail.victorkusov.contactscamera.ui;


import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gmail.victorkusov.contactscamera.R;
import com.gmail.victorkusov.contactscamera.Utils;
import com.gmail.victorkusov.contactscamera.model.ContactUser;
import com.gmail.victorkusov.contactscamera.model.CursorContactsModel;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class CameraFragment extends Fragment {

    public static final String TAG = CameraFragment.class.getSimpleName();
    private static final int REQUEST_PHOTO = 12;
    private static final int REQUEST_WRITE_STORAGE = 32;
    private static final String FILEPROVIDER_AUTHORITIES = "com.gmail.victorkusov.contactscamera.fileprovier";
    private static final String KEY_PHOTO_URI = "photo_uri";
    private static final String KEY_SAVE_BUTTON_VISIBILITY = "button_visibility";

    private ImageView mImageView;
    private TextView mTextView;
    private TextView mPhotoNotFound;
    private Button mButtonGetPhoto;
    private Button mButtonSavePhoto;
    private ProgressBar mBar;

    private ContactUser mUser;
    private static int mPosition;

    private CursorContactsModel mModel;
    private String mCurrentPhotoPath;
    private Uri mPhotoUri;
    private PhotoExportThread mExportedThread;
    private View.OnClickListener mSaveBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (Utils.isWriteStorageGranted(getContext())) {
                mBar.setVisibility(View.VISIBLE);
                mExportedThread = getExportedThread();
                mExportedThread.start();
            }
        }
    };

    public CameraFragment() {
        super();
    }

    public static CameraFragment newInstance(int position) {
        CameraFragment fragment = new CameraFragment();

        mPosition = position;

        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_camera, container, false);
        mImageView = view.findViewById(R.id.camera_photo);
        mTextView = view.findViewById(R.id.camera_txt_name);
        mPhotoNotFound = view.findViewById(R.id.camera_photo_not_found);
        mButtonGetPhoto = view.findViewById(R.id.btn_camera_new_photo);
        mButtonSavePhoto = view.findViewById(R.id.btn_camera_save);
        mBar = view.findViewById(R.id.camera_progress_bar);

        mModel = ViewModelProviders.of(getActivity()).get(CursorContactsModel.class);
        List<ContactUser> value = mModel.getContacts().getValue();
        if (value != null) {
            mUser = value.get(mPosition);

            if (savedInstanceState != null) {
                mButtonSavePhoto.setVisibility(savedInstanceState.getInt(KEY_SAVE_BUTTON_VISIBILITY, View.INVISIBLE));
                mButtonSavePhoto.setOnClickListener(mSaveBtnListener);
                mPhotoUri = Uri.parse(String.valueOf(savedInstanceState.get(KEY_PHOTO_URI)));
            } else {
                mPhotoUri = mUser.getPhotoAddress() != null ? mUser.getPhotoAddress() : mUser.getImageThumbnail();
            }

            if (mPhotoUri != null) {
                Picasso.get().load(mPhotoUri).into(mImageView);
            } else {
                mImageView.setImageResource(R.drawable.unknown_user);
            }
            mTextView.setText(mUser.getName());
            mButtonGetPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isWriteStorageGranted(getContext())) {
                        if (mExportedThread != null && mExportedThread.isAlive()) {
                            mExportedThread.interrupt();
                        }
                        takePhoto();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
                        }
                    }

                }
            });
        }
        return view;
    }

    private void takePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Context context = getContext();
        if (context != null) {
            if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (photoFile != null) {
                    Picasso.get().invalidate(mPhotoUri);
                    mPhotoUri = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                            FileProvider.getUriForFile(context, FILEPROVIDER_AUTHORITIES, photoFile) :
                            Uri.fromFile(photoFile);

                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
                    startActivityForResult(takePictureIntent, REQUEST_PHOTO);
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = SimpleDateFormat.getDateTimeInstance().format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        Context context = getContext();
        File image = null;
        if (context != null) {

            File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(imageFileName, ".jpg", storageDir);
            mCurrentPhotoPath = image.getAbsolutePath();
        }
        return image;
    }

    public void writeDisplayPhoto(long rawContactId, byte[] photo) {
        Context context = getContext();
        if (context != null) {
            ContentResolver resolver = context.getContentResolver();
            Uri rawContactPhotoUri = Uri.withAppendedPath(
                    ContentUris.withAppendedId(ContactsContract.RawContacts.CONTENT_URI, rawContactId),
                    ContactsContract.RawContacts.DisplayPhoto.CONTENT_DIRECTORY);
            try {
                AssetFileDescriptor fd =
                        resolver.openAssetFileDescriptor(rawContactPhotoUri, "rw");
                if (fd != null) {
                    OutputStream os = fd.createOutputStream();
                    os.write(photo);
                    os.close();
                    fd.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_PHOTO) {
                mUser.setPhotoAddress(mPhotoUri);
                Picasso.get().load(mPhotoUri).into(mImageView);


                mButtonSavePhoto.setVisibility(View.VISIBLE);

                mButtonSavePhoto.setOnClickListener(mSaveBtnListener);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private PhotoExportThread getExportedThread() {
        return new PhotoExportThread(new Runnable() {
            @Override
            public void run() {
                Drawable drawable = mImageView.getDrawable();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

                if (PhotoExportThread.interrupted()) {
                    Log.d(TAG, "run: thread has interrupted");
                    return;
                }
                byte[] imageBytes = stream.toByteArray();
                writeDisplayPhoto(mUser.getContactId(), imageBytes);

                if (PhotoExportThread.interrupted()) {
                    Log.d(TAG, "run: thread has interrupted");
                    return;
                }
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Context context = getActivity();
                        if (context != null) {
                            mUser.setPhotoAddress(mPhotoUri);
//                            mModel.setContact(mPosition, mUser);

                            mBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getContext(), R.string.setup_complete, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(KEY_PHOTO_URI, mPhotoUri.toString());
        outState.putInt(KEY_SAVE_BUTTON_VISIBILITY, mButtonSavePhoto.getVisibility());
        super.onSaveInstanceState(outState);
    }

    private class PhotoExportThread extends Thread {
        public PhotoExportThread(Runnable runnable) {
            super(runnable);
        }
    }
}
