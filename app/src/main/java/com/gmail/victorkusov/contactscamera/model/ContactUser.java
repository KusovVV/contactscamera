package com.gmail.victorkusov.contactscamera.model;

import android.net.Uri;

public class ContactUser {

    private long contactId;
    private String name;
    private Uri photoAddress;
    private String numbers;
    private Uri imageThumbnailUri;


    public long getContactId() {
        return contactId;
    }

    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    public Uri getImageThumbnail() {
        return imageThumbnailUri;
    }

    public void setImageThumbnail(Uri imageThumbnailUri) {
        this.imageThumbnailUri = imageThumbnailUri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Uri getPhotoAddress() {
        return photoAddress;
    }

    public void setPhotoAddress(Uri photoAddress) {
        this.photoAddress = photoAddress;
    }

    public String getNumbers() {
        return numbers;
    }

    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }

    @Override
    public String toString() {
        return "ContactUser{" +
                "contactId=" + contactId +
                ", name='" + name + '\'' +
                ", photoAddress=" + photoAddress +
                ", numbers='" + numbers + '\'' +
                ", imageThumbnailUri=" + imageThumbnailUri +
                '}';
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof ContactUser)) {
            return false;
        }
        ContactUser user = (ContactUser) obj;

        return user.getContactId() == this.contactId;
    }
}
