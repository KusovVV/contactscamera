package com.gmail.victorkusov.contactscamera.ui;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.gmail.victorkusov.contactscamera.R;
import com.gmail.victorkusov.contactscamera.Utils;
import com.gmail.victorkusov.contactscamera.model.ContactUser;
import com.gmail.victorkusov.contactscamera.model.ContactsLoader;
import com.gmail.victorkusov.contactscamera.model.CursorContactsModel;

import java.util.List;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<ContactUser>> {

    private static final String TAG = MainActivity.class.getSimpleName();


    private static final int LOADER_CONTACTS = 1;
    private static final int PERMISSIONS_REQUEST = 12;
    public static final String[] PERMISSIONS = {
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_CONTACTS};

    private CursorContactsModel mContactsModel;
    private ContentObserver mObserver = new ContentObserver(null) {
        @Override
        public void onChange(boolean selfChange) {
            Log.d(TAG, "onChange: contacts data updated");
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    getSupportLoaderManager().restartLoader(LOADER_CONTACTS, null, MainActivity.this);
                }
            });
            super.onChange(selfChange);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Utils.isReadContactsGranted(MainActivity.this)) {
            startReadContacts();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSIONS_REQUEST);
            }
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        ContactsListFragment fragmentContacts = (ContactsListFragment) fragmentManager.findFragmentByTag(ContactsListFragment.TAG);
        if (fragmentContacts == null) {
            fragmentManager
                    .beginTransaction()
                    .add(R.id.lay_frame, ContactsListFragment.newInstance(), ContactsListFragment.TAG)
                    .commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getContentResolver().registerContentObserver(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, true, mObserver);
    }

    @Override
    protected void onStop() {
        getContentResolver().unregisterContentObserver(mObserver);
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @NonNull
    @Override
    public Loader<List<ContactUser>> onCreateLoader(int id, @Nullable Bundle args) {
        Log.d(TAG, "onCreateLoader: ");

        return new ContactsLoader(this);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<List<ContactUser>> loader, List<ContactUser> data) {
        Log.d(TAG, "onLoadFinished: ");
        mContactsModel.getContacts().postValue(data);
    }


    @Override
    public void onLoaderReset(@NonNull Loader<List<ContactUser>> loader) {
        Log.d(TAG, "onLoaderReset: ");
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: ");
        if (requestCode == PERMISSIONS_REQUEST) {
            if (grantResults.length > 0) {
                boolean denied = false;
                int permissionLength = permissions.length;
                for (int i = 0; i < permissionLength; i++) {

                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        String format = String.format(
                                getResources().getString(R.string.permission_denied),
                                permissions[i]);
                        Toast.makeText(MainActivity.this, format, Toast.LENGTH_SHORT).show();
                        denied = true;
                        this.finish();
                    }
                }
                if (!denied) {
                    startReadContacts();
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void startReadContacts() {
        mContactsModel = ViewModelProviders.of(MainActivity.this).get(CursorContactsModel.class);

        if (mContactsModel.getContacts().getValue() == null) {
            getSupportLoaderManager().initLoader(LOADER_CONTACTS, null, this);
        }
    }
}
