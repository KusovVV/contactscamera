package com.gmail.victorkusov.contactscamera.ui;

import android.view.View;

interface IOnItemClickListener {
    void onClick(int position);
}
